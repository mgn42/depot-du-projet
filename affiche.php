<?php
/**
 * Created by PhpStorm.
 * User: Mathieu
 * Date: 15/03/2019
 * Time: 14:48
 */

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use factcli\models\client as Client;
use factcli\models\facture as Facture;

// connection à la base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();

//place l'ID récupérer du client dans la variable
$clientSelection = $_POST['formulaire'];
echo '<br>';
echo '<br>';

//requete pour récupérer le nom du client qui via sont ID
$nomDuClient = Client::where('id', '=', $clientSelection)->get();

//affiche le nom du client et sont ID
foreach ($nomDuClient as $affNom) {
    echo '<Strong><U>';
    echo $affNom->nomcli;
    echo '</Strong>';
    echo " (id : $affNom->id)";
    echo '</U>';
}
echo '<br>';

// requete pour récupérer les factures du client séléctionné
$factureTab = Facture::where('client_id', '=', $clientSelection)->get();

// optionnel : ajout de 2 variable pour afficher la quantité et le montant total des factures
static $compteurFacture = 0;
static $montantTotal = 0;

// affiche les numéros de factures et leurs montant associés)
foreach ($factureTab as $facture) {
    echo "Montant de la facture n° $facture->id &#8594 <strong>$facture->montant </strong> €";
    echo '<br>';
    $compteurFacture++;
    $montantTotal += $facture->montant;
}

//affiche la quantité et le montant total
echo '<br>';
echo "Quantité : $compteurFacture";
echo '<br>';
echo "Quantité : $montantTotal €";

?>