<?php

session_start();


require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;


$nomItem = strip_tags($_POST["nomItem"]);
$descItem = strip_tags($_POST["descItem"]);
$tarifItem = strip_tags($_POST["tarifItem"]);


// connection à la base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();



if($nomItem == null){
    echo "<script>alert('Entrer un nom');
    window.location.href='pageCreerItem.php';
    </script>";
}
else

if($descItem == null){
    echo "<script>alert('Entrer une description');
    window.location.href='pageCreerItem.php';
    </script>";
}
else
if($tarifItem == 0){
    echo "<script>alert('Entrer un prix supérieur à 0 €');
    window.location.href='pageCreerItem.php';
    </script>";
}
else


// ATTENTION OBLIGE DE DONNER UN LISTE_ID
db::table('item')->insert([ 'nom' => "$nomItem", 'descr' => "$descItem", 'tarif' =>"$tarifItem"/*, 'img' =>"$image"*/]);

echo "<script>alert('ITEM ajouté mais mauvaise saisie');
    window.location.href='pageCreerItem.php';
    </script>";



