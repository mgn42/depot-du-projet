<?php

session_start();
require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use wishlisttest\conf\Outils as Outils;
Outils::headerHTML("ajout Liste");

$nomListe = $_SESSION["nomListePourAjout"];
$descriptionListe = $_SESSION["descListePourAjout"];
$userID = $_SESSION["id"];
$contenuListe = $_SESSION["contenuListePourAjout"];

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();


//ajout de la nouvelle liste
db::table('liste')->insert(['user_id' => $userID, 'titre' => $nomListe, 'description' => $descriptionListe,]);
$idListe = db::getPdo()->lastInsertId(); //recupere l'id du dernier enregistrement

//ajout des items de la nouvelle liste (voir tableau jonction)
foreach ($contenuListe as $itemSelect ){
    if($itemSelect["quantity"] == null){;}
    else {

        echo $itemSelect["name"];
        echo '<br>';
        echo $itemSelect["id"];
        // $recupeItem = Item::where('id', '=', $itemSelect["id"])->get();

        db::table('link')->insert(["liste" => $idListe, "item" =>$itemSelect["id"], "quantiter" => $itemSelect["quantity"]  ]);
    }
}

echo "<script>alert('Liste ajouté');
    window.location.href='pageMembre.php';
    </script>";


Outils::footerHTML();