<?php
session_start();


require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use wishlisttest\conf\Outils as Outils;
Outils::headerHTML("creation item");

// connection à la base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();
echo '<h1>Ajout d un item</h1>
        <br>
        <br>

<form action = "pageAjoutItemBD.php" method ="post">
   <label>Nom</label>
    <input type = "text" name = "nomItem">
    <br>
    <br>
    <label>Description </label>
    <input type = "text" name = "descItem" size ="80" >
    <br>
    <br>
    <label>Tarif </label>
    <input type = "number" name = "tarifItem" min = "0" max = "999999" step = "0.01">
    <br>
    <br>
    <br>
    <input type = "submit" value = "Ajouter">
    </form>
';


echo '<form id="listeItemChoix" action="pageCreerListe.php" method="post">';
echo '<input type="submit" value="Annuler"/>
    </form>';


Outils::footerHTML();