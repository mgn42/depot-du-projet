<?php
require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use wishlisttest\models\User as User;
use wishlisttest\conf\Outils as Outils;
Outils::headerHTML("Ajout utlisateur");


$mail = strip_tags($_POST["inscriptionMail"]); // A FAIRE : verifier le format
$nom = strip_tags($_POST["inscriptionNOM"]);
$prenom = strip_tags($_POST["inscriptionPrenom"]);
$date = strip_tags($_POST["inscriptionDate"]);
$mdp = strip_tags($_POST["inscriptionMDP"]);
$hashMDP = password_hash($mdp,PASSWORD_BCRYPT );


//verifie le format du mail
if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
    echo "<script>alert('Entrer un email valide');
    window.location.href='index.html';
    </script>";
} else if ($nom == null) {
    echo "<script>alert('Entrer votre nom');
    window.location.href='index.html';
    </script>";
} else if ($prenom == null) {
    echo "<script>alert('Entrer votre prenom');
    window.location.href='index.html';
    </script>";
} //verifie si le mot de passe à plus de 5 caractere
else
    if (strlen($mdp) < 6) {
        echo "<script>alert('Entrer un mot de passe supérieur à 5 caractere');
    window.location.href='index.html';
    </script>";
    } else
        $db = new DB();

$db->addConnection(parse_ini_file('src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();

echo $hashMDP;

$recupeUtilisateur = User::where('email', '=', $mail)->first();

if (!$recupeUtilisateur == null) {
    echo "<script>alert('Email deja utilisé');
    window.location.href='index.html';
    </script>";
} else {
    db::table('user')->insert(['nom' => "$nom", 'prenom' => "$prenom", 'email' => "$mail", 'naissance' => "$date", 'password' => $hashMDP]);
}


echo "<script>alert('Inscription terminée');
    window.location.href='index.html';
    </script>";
Outils::footerHTML();