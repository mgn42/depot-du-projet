<?php
session_start();
/**
 * Created by PhpStorm.
 * User: Mathieu
 * Date: 15/03/2019
 * Time: 14:48
 */

require_once 'vendor/autoload.php';

use wishlisttest\conf\Outils as Outils;
Outils::headerHTML("confirm item");

$nomListe = strip_tags($_GET["nomListe"]);
$descListe = strip_tags($_GET["descriptionListe"]);
$choix = $_GET["choice"];   // injection possible avec le tableau choice
$_SESSION["contenuListePourAjout"] = $_GET["choice"];
$_SESSION["nomListePourAjout"] = $nomListe ;
$_SESSION["descListePourAjout"] = $descListe;

$totalItem = 0;
$totalPrix = 0;

if($nomListe == null){
    echo "<script>alert('Entrer un nom de liste');
window.location.href='pageCreerListe.php';
</script>";
}
else
    echo '<h1>Récupitulatif</h1>
       <p><strong> Nom de la liste : </strong> '.$nomListe.'</p>
       <p><strong> Description : </strong> '.$descListe.'</p>
        ';


foreach ($choix as $itemSelect) {
    if ($itemSelect["quantity"] == null) {
        ;
    } else {
        $totalItem += $itemSelect["quantity"];
        $totalPrix = $totalPrix + ((double)$itemSelect["price"] * (int)$itemSelect["quantity"]);
        echo '<br>';
        echo $itemSelect["quantity"] . " x " . $itemSelect["name"] . "(" . $itemSelect["price"] . " / unité)  => " . ((double)$itemSelect["price"] * (int)$itemSelect["quantity"]);
        echo '<br>';
    }
}
echo ' <p><strong> Quantité : </strong> '.$totalItem.'</p>
       <p><strong> Prix total : </strong> '.$totalPrix.' €</p>
        ';

echo '<form method = "get" action = "pageAjoutListeBD.php"> ';
echo '<input type = "Submit" value = "Confirmer">';
echo '</form>';

echo '<form method = "get" action = "pageCreerListe.php"> ';
echo '<input type = "Submit" value = "Annuler">';
echo '</form>';

echo '<form method = "get" action = "index.html"> ';
echo '<input type = "Submit" value = "Accueil">';
echo '</form>';
Outils::footerHTML();
