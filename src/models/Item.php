<?php
/**
 * User: gautier
 */

namespace wishlisttest\models;

class Item extends \Illuminate\Database\Eloquent\Model

{
    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public function lienLi()
    {
        return $this->belongsTo('\model\Liste', 'liste_id');
    }
}
?>