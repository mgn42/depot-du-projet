<?php
/**
 * Created by PhpStorm.
 * User: Mathieu
 * Date: 11/03/2019
 * Time: 15:26
 */

namespace wishlisttest\models;


class Client extends \Illuminate\Database\Eloquent\Model
{
    protected  $table = 'client';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function client(){
        return $this->hasMany('\model\Facture', 'client_id');
    }
}
?>