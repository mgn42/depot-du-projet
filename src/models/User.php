<?php
/**
 * User: gautier
 */

namespace wishlisttest\models;

class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function lienLi()
    {
        return $this->hasMany('\model\Liste', 'user_id');
    }
}
?>