<?php
/**
 * User: gautier
 */

namespace wishlisttest\models;

class Liste extends \Illuminate\Database\Eloquent\Model

{
    protected $table = 'liste';
    protected $primaryKey = 'no';
    public $timestamps = false;


    public function lienIt()
    {
        return $this->hasMany('\model\Item', 'liste_id');
    }

    public function lienUs()
    {
        return $this->belongsTo('\model\Liste', 'user_id');
    }
}
?>