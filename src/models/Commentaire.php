<?php
/**
 * User: gautier
 */

namespace wishlisttest\models;

class Commentaire extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'commentaire';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function lienIt()
    {
        return $this->belongsTo('\model\User', 'user');
    }

    public function lienUs()
    {
        return $this->belongsTo('\model\Liste', 'listeCom');
    }
}
?>