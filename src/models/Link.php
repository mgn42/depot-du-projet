<?php
/**
 * User: gautier
 */

namespace wishlisttest\models;

class Link extends \Illuminate\Database\Eloquent\Model

{
    protected $table = 'link';
    protected $primaryKey = 'idLink';
    public $timestamps = false;


    public function lienIt()
    {
        return $this->belongsTo('\model\Item', 'liste');
    }
}
?>