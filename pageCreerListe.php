<?php
/**
 * Created by PhpStorm.
 * User: Mathieu
 * Date: 15/03/2019
 * Time: 14:48
 */
session_start();


require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use wishlisttest\models\User as User;
use wishlisttest\models\Liste as Liste;
use wishlisttest\models\Item as Item;
use wishlisttest\conf\Outils as Outils;
Outils::headerHTML("création Liste");


// connection à la base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$recupeItem = Item::all();



echo '<form id="listeItemChoix" action="confirmerChoixItem.php" method="get">';
$compteur = 0;
echo '<p><strong>Nom de la liste : </strong></p>';
echo '<input type="text" name = "nomListe"> <br><br>';
echo '<p><strong>Description : </strong></p>';
echo '<input type="text" name = "descriptionListe" spellcheck="true" style = "width: 400px"> <br><br>';
echo '<br>
';
//***********************LISTE*********************
foreach ($recupeItem as $item) {

    echo '<div class="form-item" class="boxItem">
            
                
                    <input type="number" name="choice[' . $compteur . '][quantity]" placeholder ="quantité" min = "0" max = "100"/>
                    <input type="hidden" name="choice[' . $compteur . '][price]" value="' . $item->tarif . '" />
                    <input type="hidden" name="choice[' . $compteur . '][name]" value="' . $item->nom . '" />
                    <input type="hidden" name="choice[' . $compteur . '][id]" value="' . $item->id . '" />
                    <input type="hidden" name="choice[' . $compteur . '][description]" value="' . $item->desc . '" />
                       
                    ' .
        $item->nom . '  ' . $item->tarif . ' €'.'/  Description: '.$item->descr.'
        </div>';
    $compteur++;
}
//*******************BOUTON ORDER**********************
echo '<input type="submit" value="Suivant"/>
    </form>';


echo '<form id="listeItemChoix" action="pageCreerItem.php" method="post">';
echo '<input type="submit" value="Créer un nouveau item"/>
    </form>';


//******************BOUTON RETOUR**********************
echo '<form id="listeItemChoix" action="pageMembre.php" method="post">';
echo '<input type="submit" value="Annuler"/>
    </form>';


Outils::footerHTML();
