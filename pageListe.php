<?php

session_start();

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use wishlisttest\models\Liste as Liste;
use wishlisttest\conf\Outils as Outils;
Outils::headerHTML("Liste");

//connection à la BD
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();

//recupere toute les listes de l'utilisateur connecté
$recupeListes = Liste::where('user_id', '=', $_SESSION["id"])->get();


echo '<h1>';
echo "Mes listes ";
echo '</h1>';
echo '<form method = "post" action = "pageListe.php"> ';
echo '<type = "Submit" value = "Ouvrir celle liste">';
echo '</form>';

echo '<br>';

//verifie la presence de liste
if ($recupeListes[0] == null) {
    echo "<script>alert('Aucune liste enregistré sur votre compte');
    window.location.href='pageMembre.php';
    </script>";
} else {

    $compteur = 1;
    echo '<form method="get" action = "pageListeConsultation.php">';
    foreach ($recupeListes as $liste) {
        echo  $compteur . '
         <input type="radio" name="choixListe" value = "' . $liste->no . '"
         <p>' . $liste->titre . '</p>';
        $compteur++;
    }
}

echo '<input type ="Submit" value = Consulter la liste>
    </form>
    
    <form  method="post" action="pageMembre.php">
    <input type ="Submit" value = "Retour"/>
    </form>';

Outils::footerHTML();