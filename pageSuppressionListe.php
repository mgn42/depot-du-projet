<?php

session_start();


require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use wishlisttest\models\Liste as Liste;
use wishlisttest\models\Link as Link;
use wishlisttest\conf\Outils as Outils;

Outils::headerHTML("DEUS VULT!");
// connection à la base de donnée
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();

echo $_POST["idListeSuppression"];

$listeID = Link::where("liste", "=" , $_POST["idListeSuppression"])->get();
foreach ($listeID as $liste){

    Link::destroy($liste->idLink);

}
Liste::destroy($_POST["idListeSuppression"]);




